/*CRUD Operations
	Create - Insert
	Read - Find
	Update
	Delete
*/

/*Inserting Documents (Create)
	
	Syntax:
	db.collectionName.insertOne({object});

	JS:
	
*/
db.users.insertOne({
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "contact": {
                "phone": "09191234567",
                "email": "janedoe@mail.com"
            },
        "courses": ["CSS", "JavaScript", "Python"],
        "department": "none"
});

/*Insert Many
	Syntax:
	db.collectionName.insertMany({objectA}, {objectB});
*/

db.users.insertMany([{
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
                "phone": "09191234567",
                "email": "stephenhawking@mail.com"
            },
        "courses": ["React", "PHP", "Python"],
        "department": "none"
}, 
{
        "firstName": "Niel",
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
                "phone": "09201234567",
                "email": "nielarmstrong@mail.com"
            },
        "courses": ["React", "Laravel", "SASS"],
        "department": "none"
}
]);

/*Finding documents (Read) Operation
	
	Find Syntax:
	db.collectionName.find();
	db.collectionName.find({field: value});
	
*/
db.users.find();
db.users.find({"lastName": "Doe"});
db.users.find({"lastName": "Doe", "age": 25}).pretty();

/*Updating Documents (Update) Operation
	
	Syntax:
	db.collectionName.updateOne({criteria}, {$set: {field: value}});
*/
// Insert Test Document
db.users.updateOne({
        "firstName": "Test",
        "lastName": "Test",
        "age": 0,
        "contact": {
                "phone": "00000000000",
                "email": "test@mail.com"
            },
        "courses": [],
        "department": "none"
});

// Updated One Document
db.users.updateOne({"firstName": "Test"}, {$set: {
        "firstName": "Bill",
        "lastName": "Gates",
        "age": 65,
        "contact": {
                "phone": "09171234567",
                "email": "bill@mail.com"
            },
        "courses": ["PHP", "Laravel", "HTML"],
        "department": "none"
}});

// Update multiple documents
db.users.updateMany({"department": "none"}, {$set:{"department": "HR"}});

/*Deleting Documents (Delete) Operation
	
	Syntax:
	db.collectionName.deleteOne({criteria}); - deleting a single document
*/ 

// Creat a single document with one field
db.users.insert({
    "firstName": "test"
});

// Deleting One Document
db.users.deleteOne({
    "firstName": "test"
});

// Deleting Many Documents
db.users.deleteMany(
    {"department": "Operations"}
);

// Advance Query

// Query an embedded document
db.users.find({
	"contact": {
		"phone": "09171234567",
		"email": "bill@mail.com"
	}
});

// Querying an array without a specific order of elements
db.users.find({"courses":{ $all: ["React", "Python"]}});